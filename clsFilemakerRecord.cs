﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncFilemaker
{
    public class FilemakerRecord
    {

        public FilemakerRecord()
        {

        }

        public int record_id { get; set; }
        public int mod_id { get; set; }

        public string pop_date { get; set; }
        public string pop_time { get; set; }
        public string pop_date_text { get; set; }

        public string id_num { get; set; }
        public string unique_id { get; set; }
        public string company { get; set; }
        public string campaign_id { get; set; }
        public string campaign { get; set; }
        public string campaign_name { get; set; }

        public string fm_timestamp { get; set; }
        public string fm_timestamp_calc { get; set; }

        public string url_from { get; set; }
        public string site_id { get; set; }
        public string site { get; set; }
        public string search_engine { get; set; }

        public string campus_parsed { get; set; }
        public string campus { get; set; }
        public string program_parsed { get; set; }
        public string program { get; set; }
        public string education_parsed { get; set; }
        public string education { get; set; }
        public string graduation_parsed { get; set; }
        public string graduation { get; set; }
        public string begin_study_parsed { get; set; }
        public string begin_study { get; set; }
        public string contact_time_parsed { get; set; }
        public string contact_time { get; set; }
        public string consent { get; set; }

        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state_parsed { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string phone_1 { get; set; }
        public string phone_2 { get; set; }

        public string sce { get; set; }
        public string source_code { get; set; }
        public string source { get; set; }
        public string billable { get; set; }
        public string billable_notes { get; set; }

        public string lead_provider { get; set; }
        public string lead_type { get; set; }

        
        public string pop_body_type { get; set; }
        public string pop_body { get; set; }
        public string pop_from { get; set; }
        public string pop_full_headers { get; set; }
        public string pop_to { get; set; }
        public string pop_subject { get; set; }

        public string smtp_from { get; set; }
        public string smtp_subject { get; set; }
        public string smtp_to { get; set; }
        public string smtp_body { get; set; }

        public string phone_1_label { get; set; }
        public string phone_2_label { get; set; }

        public string field_18_label { get; set; }
        public string field_19_label { get; set; }
        public string field_20_label { get; set; }
        public string field_21_label { get; set; }
        public string field_22_label { get; set; }
        public string field_23_label { get; set; }

        public string field_18 { get; set; }
        public string field_19 { get; set; }
        public string field_20 { get; set; }
        public string field_21 { get; set; }
        public string field_22 { get; set; }
        public string field_23 { get; set; }

        public string network { get; set; }
        public string keyword { get; set; }
        public string keywords { get; set; }
        public string ip_address { get; set; }
        public string url_form { get; set; }
        public string url_initial { get; set; }
        public string session_id { get; set; }
        public string customer_record_id { get; set; }
        public string customer_record_id_2 { get; set; }
        public string referred_by { get; set; }

        public string esmid { get; set; }

        public DateTime timestamp_to_use
        {
            get
            {
                if (!string.IsNullOrEmpty(fm_timestamp))
                {
                    return DateTime.Parse(fm_timestamp);
                }
                else if (!string.IsNullOrEmpty(fm_timestamp_calc))
                {
                    return DateTime.Parse(fm_timestamp_calc);
                }
                else
                {
                    return pop_datetime;
                }
            }
        }

        public string keyword_to_use
        {
            get
            {
                if (!string.IsNullOrEmpty(keyword))
                {
                    return keyword;
                }
                else if (!string.IsNullOrEmpty(keywords))
                {
                    return keywords;
                }
                else
                {
                    return null;
                }
            }
        }

        public string customer_record_id_to_use
        {
            get
            {
                string _val = customer_record_id;
                if (string.IsNullOrEmpty(_val))
                {
                    _val = getValueFromPopBody("Catalysis Lead ID :", "\n");                    
                }
                return _val;
            }
        }
        public string campaign_to_use
        {
            get
            {
                if (!string.IsNullOrEmpty(campaign))
                {
                    return campaign;
                }
                else if (!string.IsNullOrEmpty(campaign_name))
                {
                    return campaign_name;
                }
                else
                {
                    return null;
                }
            }
        }
        public string source_to_use
        {
            get
            {
                if (!string.IsNullOrEmpty(source))
                {
                    return source;
                }
                else if (!string.IsNullOrEmpty(search_engine))
                {
                    return search_engine;
                }
                else
                {
                    return null;
                }
            }
        }
        public DateTime pop_datetime
        {
            get
            {
                return Convert.ToDateTime(pop_date + ' ' + pop_time);
            }

        }

        public DateTime pop_datetime_header
        {
            get
            {
                string _parsedDate = GetDateFromHeader(pop_full_headers);
                if (String.IsNullOrEmpty(_parsedDate))
                {
                    _parsedDate = pop_date;
                }
                _parsedDate = _parsedDate.Replace("-0400 (EDT)", "-0400");

                DateTime _rtnDate;
                if (!DateTime.TryParse(_parsedDate, out _rtnDate)) {
                    _rtnDate = DateTime.Parse(pop_date);
                }

                return _rtnDate;

            }
        }

        public string pop_datetime_header_text
        {
            get
            {
                string _parsedDate = GetDateFromHeader(pop_full_headers);
                return _parsedDate;

            }
        }
        private string GetDateFromHeader(string _header)
        {
            string[] _lines = _header.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

            string _date = "";

            for (int i =0; i < _lines.Length; i++)
            {
                if (_lines[i].StartsWith("Date:"))
                {
                    _date = _lines[i].Substring(5).Trim();
                    break;
                }
            }


            return _date;
        }

        private string getValueFromPopBody(string _startPattern, string _endPattern)
        {
            string _val = "";
            int p9 = pop_body.IndexOf(_startPattern);
            if (p9>=0)
            {
                int p8 = pop_body.IndexOf(_endPattern, p9 + _startPattern.Length);
                if (p8>0)
                {
                    _val = pop_body.Substring(p9 + _startPattern.Length, p8 - (p9 + _startPattern.Length));
                }
            }
            return _val.Trim();
        }
    }
    
}
