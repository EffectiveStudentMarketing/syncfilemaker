﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESMCommon.Helpers;
using System.Data;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Net.Http;
using System.Web;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;


namespace SyncFilemaker
{
    class Program
    {
        private static Logger _logger = new Logger();
        private static DateTime _startdate = DateTime.Now;
        private static DateTime _enddate = DateTime.Now;
        private static SqlConnection conn;

        static void Main(string[] args)
        {
            _logger.Info("Starting");

            if (Init())
            { 
                _startdate = new DateTime(_startdate.Year, _startdate.Month, 1); ;
                _enddate = DateTime.Now;

                _startdate = new DateTime(2016, 1, 1);
                _enddate = DateTime.Now;
                                
                processXML();

                conn.Close();
            }

            _logger.Info("Ending");

            Console.Write("Hit any key to end...");
            Console.Read();
        }

        private static Boolean Init()
        {
            //Open DB Connection
            string _connectionString = ConfigurationManager.ConnectionStrings["esmdb"].ConnectionString;
            conn = new SqlConnection();

            if (!String.IsNullOrEmpty(_connectionString))
            {
                conn.ConnectionString = _connectionString;
                try
                {
                    conn.Open();
                }
                catch (Exception ex)
                {
                    _logger.Error("Error Opening DB Connection: " + ex.Message);
                    conn = null;
                    return false;
                }
            }
            else
            {
                _logger.Error("Missing DB Connection String");
                return false;
            }

            _logger.Info("Opened DB Connection");
            return true;
        }

        static void processXML()
        {
            // Get list of databases
            string[] dbs = getFilemakerDBList();

            
            // clear work table
            ClearWorkTable(conn);


            // Process them one at a time
            bool _ok = false;
            foreach (string db in dbs)
            {
                _ok = processDB(db, conn);
                if (!_ok)
                {
                    _logger.Error(String.Format("Failed to Load Database: {0}", db));
                    break;
                }
                else
                {
                    _logger.Info(String.Format("Completed Database: {0}", db));
                }
            }


            // Completion
            if (_ok)
            {
                // DoFinalProcessing(conn);
            }
            else
            {
                _logger.Error("Unable to Complete Final Processing");
            }

            // Notification
            // TODO: SendNotification
            

            // cleanup
            conn.Close();

        }

        static void ClearWorkTable(SqlConnection conn)
        {
            _logger.Info("Clearing Work Table: Work_Filemaker_XML: Start");

            string _commandText = "truncate table Work_Filemaker_XML";
            using (SqlCommand _command = new SqlCommand(_commandText, conn))
            {
                _command.ExecuteNonQuery();
            }
            _logger.Info("Clearing Work Table: Work_Filemaker_XML: End");

        }

        static bool DoFinalProcessing(SqlConnection conn)
        {
            bool _ok = false;

            _logger.Info("Final Processing: Start");

            string _commandText = "sp_SyncFilemaker";
            using (SqlCommand _command = new SqlCommand(_commandText, conn))
            {
                try
                {
                    _command.ExecuteNonQuery();
                    _ok = true;
                }
                catch( Exception ex)
                {
                    _logger.Error(string.Format("Error: Final Processing: {0}", ex.Message));
                }
            }
            _logger.Info("Final Processing: End");

            return _ok;

        }

        static string[] getFilemakerDBList()
        {
            // return new string[] { "AmericanCareerCollege", "WestCoastUniversity", "WestCoastUniversityOnline", "SalonSuccessAcademy" };

            // return new string[] { "WestCoastUniversityOnline" };
            // return new string[] { "AmericanCareerCollege", "WestCoastUniversity", "WestCoastUniversityOnline" };

            // return new string[] { "WentworthInstituteofTechnology" };


            string _dblist = ConfigurationManager.AppSettings["FilemakerDatabases"];
            string[] _dbarray = _dblist.Replace(" ","").Split(',');
            return _dbarray;
            
        }

        static XDocument GetFilemakerXMLRequest(string requestUrl)
        {
            _logger.Info("Getting XML: Start");
            try
            {
                HttpWebRequest _request = (HttpWebRequest)WebRequest.Create(requestUrl);

                _request.Credentials = new NetworkCredential("filemaker", "esmfm9");

                // SetBasicAuthHeader(request, "filemaker", "esmfm9");
                    
                XDocument xDoc = new XDocument();
                using (HttpWebResponse _response = (HttpWebResponse)_request.GetResponse())
                {
                    using (Stream _stream = _response.GetResponseStream())
                    {
                        xDoc = XDocument.Load(_stream);
                        
                    }

                }

                _logger.Info("Getting XML: End");

                return (xDoc);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Unable to Get XML request: {0}, {1}", requestUrl, ex.Message));
            }

            

        }

        static void SetBasicAuthHeader(WebRequest req, String userName, String userPassword)
        {
            string authInfo = userName + ":" +userPassword;
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            req.Headers["Authorization"] = "Basic " +authInfo;
        }

        static XDocument RemoveNamespace(XDocument xdoc)
        {
          foreach (XElement e in xdoc.Root.DescendantsAndSelf())
          {
              if (e.Name.Namespace != XNamespace.None)
              {
                  e.Name = XNamespace.None.GetName(e.Name.LocalName);
              }
              if (e.Attributes().Where(a => a.IsNamespaceDeclaration || a.Name.Namespace != XNamespace.None).Any())
              {
                  e.ReplaceAttributes(e.Attributes().Select(a => a.IsNamespaceDeclaration? null : a.Name.Namespace != XNamespace.None? new XAttribute(XNamespace.None.GetName(a.Name.LocalName), a.Value) : a));
              }
          }
        
          return xdoc;
        }

        static string buildCommandInsert()
        {
            string commandText = @"insert into Work_Filemaker_XML 
                    (   
                        work_record_id, 
                        work_mod_id, 
                        work_database, 
                        work_xml, 

                        work_id_num,
                        work_unique_id,
                        work_company,
                        work_campaign_id,
                        work_campaign,
                        work_fm_timestamp,
                        work_url_from,
                        work_site_id,
                        work_site,
                        work_search_engine,
                        
                        work_campus_parsed,
                        work_campus, 
                        work_program_parsed,
                        work_program, 
                        work_education_parsed,
                        work_education, 
                        work_graduation_parsed,
                        work_graduation, 
                        work_begin_study_parsed,
                        work_begin_study, 
                        work_contact_time_parsed,
                        work_contact_time, 
                        work_consent,

                        work_sce,
                        work_source_code,
                        work_source,
                        work_billable,
                        work_billable_notes,
                        work_lead_provider,
                        work_lead_type,

                        work_first_name, 
                        work_last_name,
                        work_email,
                        work_address,
                        work_address2,
                        work_city,
                        work_state_parsed,
                        work_state,
                        work_zip,
                        work_latitude,
                        work_longitude,
                        work_phone_1,
                        work_phone_2,

                        work_pop_datetime,
                        work_pop_datetime_header,
                        work_pop_datetime_header_text,
                        work_pop_date_text,
                        work_pop_body_type,
                        work_pop_body,
                        work_pop_from,
                        work_pop_full_headers,
                        work_pop_to,
                        work_pop_subject,

                        work_smtp_from,
                        work_smtp_subject,
                        work_smtp_to,
                        work_smtp_body,

                        work_phone_1_label,
                        work_phone_2_label,
                        work_field_18_label,
                        work_field_19_label,
                        work_field_20_label,
                        work_field_21_label,
                        work_field_22_label,
                        work_field_23_label,
                        work_field_18,
                        work_field_19,
                        work_field_20,
                        work_field_21,
                        work_field_22,
                        work_field_23,
                        work_network,
                        work_keyword,
                        work_ip_address,
                        work_url_form,
			            work_url_initial,
			            work_session_id,
			            work_customer_record_id,
			            work_customer_record_id_2,
                        work_referred_by,
                        work_esmid

                    )   values (
                
                        @work_record_id, 
                        @work_mod_id, 
                        @work_database, 
                        @work_xml, 
                        
                        @work_id_num,
                        @work_unique_id,
                        @work_company,
                        @work_campaign_id,
                        @work_campaign,
                        @work_fm_timestamp,
                        @work_url_from,
                        @work_site_id,
                        @work_site,
                        @work_search_engine,

                        @work_campus_parsed,
                        @work_campus,
                        @work_program_parsed,
                        @work_program,
                        @work_education_parsed,
                        @work_education,
                        @work_graduation_parsed,
                        @work_graduation,
                        @work_begin_study_parsed,
                        @work_begin_study,
                        @work_contact_time_parsed,
                        @work_contact_time,
                        @work_consent,

                        @work_sce,
                        @work_source_code,
                        @work_source,
                        @work_billable,
                        @work_billable_notes,
                        @work_lead_provider,
                        @work_lead_type,

                        @work_first_name, 
                        @work_last_name,
                        @work_email,
                        @work_address,
                        @work_address2,
                        @work_city,
                        @work_state_parsed,
                        @work_state,
                        @work_zip,
                        @work_latitude,
                        @work_longitude,
                        @work_phone_1,
                        @work_phone_2,

                        @work_pop_datetime,
                        @work_pop_datetime_header,
                        @work_pop_datetime_header_text,
                        @work_pop_date_text,
                        @work_pop_body_type,
                        @work_pop_body,
                        @work_pop_from,
                        @work_pop_full_headers,
                        @work_pop_to,
                        @work_pop_subject,

                        @work_smtp_from,
                        @work_smtp_subject,
                        @work_smtp_to,
                        @work_smtp_body,

                        @work_phone_1_label,
                        @work_phone_2_label,
                        @work_field_18_label,
                        @work_field_19_label,
                        @work_field_20_label,
                        @work_field_21_label,
                        @work_field_22_label,
                        @work_field_23_label,
                        @work_field_18,
                        @work_field_19,
                        @work_field_20,
                        @work_field_21,
                        @work_field_22,
                        @work_field_23,

                        @work_network,
                        @work_keyword,
                        @work_ip_address,

                        @work_url_form,
                        @work_url_initial,
                        @work_session_id,
                        @work_customer_record_id,
                        @work_customer_record_id_2,
                        @work_referred_by,

                        @work_esmid


                )";

            return commandText;
        }
        static bool processDB(string db, SqlConnection conn)
        {
            _logger.Info(string.Format("Processing Database: {0}", db));
            string _url = getFilemakerXMLURL(db);
            _logger.Info(string.Format("URL: {0}", _url));

            int _count = 0;
            bool _error = false;
            int _current_record_id = 0;

            try
            {
                XDocument _xdoc = RemoveNamespace(GetFilemakerXMLRequest(_url));

                // XmlNamespaceManager _namespace = new XmlNamespaceManager(_xdoc.NameTable);
                // _namespace.AddNamespace("fmresultset", "http://www.filemaker.com/xml/fmresultset");


                // save to working directory
                string _workdir = ConfigurationManager.AppSettings["WorkingDirectory"] ?? "c:\\temp\\";
                string _filename = db + ".xml";
                string _filepath = _workdir + _filename;

                _xdoc.Save(_filepath);
                _logger.Info(string.Format("Saved File: {0}", _filepath));


                // write to work table

                string commandText = buildCommandInsert();

                //XmlNode _resultset = _xdoc.SelectSingleNode("//fmresultset:resultset", _namespace);

                IEnumerable<FilemakerRecord> _query = GetFilemakerRecords(_xdoc);

                if (_query.Count()>0)
                {

                    _logger.Info(string.Format("Found Records: {0}", _query.Count()));

                    foreach (var _record in _query)
                    {
                        _count++;
                        _current_record_id = _record.record_id;
                        if (_current_record_id == 229292)
                        {
                            _logger.Info(string.Format("Record ID: {0}", _current_record_id));
                        }

                        if (_count % 50 == 0)
                        {
                            _logger.Info(string.Format("Records Processed: {0}", _count));

                        }
                        string _xml = _xdoc.XPathSelectElement("/fmresultset/resultset/record[@record-id=" + _record.record_id.ToString() + "]").ToString();

                        using (SqlCommand _command = new SqlCommand(commandText, conn))
                        {

                            string _firstname = "";
                            if (!string.IsNullOrEmpty(_record.first_name))
                            {
                                _firstname = _record.first_name;
                                if (_firstname.Length>100)
                                {
                                    _firstname = _firstname.Substring(0, 100);
                                }
                            }
                            string _lastname = "";
                            if (!string.IsNullOrEmpty(_record.last_name))
                            {
                                _lastname = _record.last_name;
                                if (_lastname.Length > 100)
                                {
                                    _lastname = _lastname.Substring(0, 100);
                                }
                            }
                            _command.Parameters.AddWithValue("@work_record_id", _record.record_id);
                            _command.Parameters.AddWithValue("@work_mod_id", _record.mod_id);
                            _command.Parameters.AddWithValue("@work_database", db);
                            _command.Parameters.AddWithValue("@work_xml", _xml);

                            _command.Parameters.AddWithValue("@work_id_num", _record.id_num);
                            _command.Parameters.AddWithValue("@work_unique_id", _record.unique_id ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_company", _record.company);
                            _command.Parameters.AddWithValue("@work_campaign_id", _record.campaign_id ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_campaign", _record.campaign_to_use ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_fm_timestamp", _record.timestamp_to_use);
                            _command.Parameters.AddWithValue("@work_url_from", _record.url_from ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_site_id", _record.site_id ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_site", _record.site ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_search_engine", _record.search_engine ?? (object)DBNull.Value);

                            _command.Parameters.AddWithValue("@work_campus_parsed", _record.campus_parsed ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_campus", _record.campus ?? (object)DBNull.Value ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_program_parsed", _record.program_parsed ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_program", _record.program ?? (object)DBNull.Value ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_education_parsed", _record.education_parsed ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_education", _record.education ?? (object)DBNull.Value ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_graduation_parsed", _record.graduation_parsed ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_graduation", _record.graduation ?? (object)DBNull.Value ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_begin_study_parsed", _record.begin_study_parsed ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_begin_study", _record.begin_study ?? (object)DBNull.Value ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_contact_time_parsed", _record.contact_time_parsed ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_contact_time", _record.contact_time ?? (object)DBNull.Value ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_consent", _record.consent ?? (object)DBNull.Value);

                            _command.Parameters.AddWithValue("@work_sce", _record.sce ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_source_code", _record.source_code ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_source", _record.source_to_use ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_billable", _record.billable ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_billable_notes", _record.billable_notes ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_lead_provider", _record.lead_provider ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_lead_type", _record.lead_type ?? (object)DBNull.Value);

                            _command.Parameters.AddWithValue("@work_first_name", _firstname ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_last_name", _lastname ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_email", _record.email ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_address", _record.address ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_address2", _record.address2 ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_city", _record.city ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_state_parsed", _record.state_parsed ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_state", _record.state ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_zip", _record.zip ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_latitude", _record.latitude ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_longitude", _record.longitude ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_phone_1", _record.phone_1 ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_phone_2", _record.phone_2 ?? (object)DBNull.Value);

                            _command.Parameters.AddWithValue("@work_pop_datetime", _record.pop_datetime);
                            _command.Parameters.AddWithValue("@work_pop_datetime_header", _record.pop_datetime_header);
                            _command.Parameters.AddWithValue("@work_pop_datetime_header_text", _record.pop_datetime_header_text);
                            _command.Parameters.AddWithValue("@work_pop_date_text", _record.pop_date_text ?? (object)DBNull.Value);

                            _command.Parameters.AddWithValue("@work_pop_body_type", _record.pop_body_type ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_pop_body", _record.pop_body ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_pop_from", _record.pop_from ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_pop_full_headers", _record.pop_full_headers ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_pop_to", _record.pop_to ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_pop_subject", _record.pop_subject ?? (object)DBNull.Value);

                            _command.Parameters.AddWithValue("@work_smtp_from", _record.smtp_from ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_smtp_subject", _record.smtp_subject ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_smtp_to", _record.smtp_to ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_smtp_body", _record.smtp_body ?? (object)DBNull.Value);

                            _command.Parameters.AddWithValue("@work_phone_1_label", _record.phone_1_label ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_phone_2_label", _record.phone_2_label ?? (object)DBNull.Value);

                            _command.Parameters.AddWithValue("@work_field_18_label", _record.field_18_label ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_field_19_label", _record.field_19_label ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_field_20_label", _record.field_20_label ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_field_21_label", _record.field_21_label ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_field_22_label", _record.field_22_label ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_field_23_label", _record.field_23_label ?? (object)DBNull.Value);

                            _command.Parameters.AddWithValue("@work_field_18", _record.field_18 ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_field_19", _record.field_19 ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_field_20", _record.field_20 ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_field_21", _record.field_21 ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_field_22", _record.field_22 ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_field_23", _record.field_23 ?? (object)DBNull.Value);

                            string _network = null; 
                            if (!string.IsNullOrEmpty(_record.network))
                            {
                                _network = _record.network;
                                if (_network.Length>50)
                                {
                                    _network = _network.Substring(0, 50);
                                }
                            }
                            _command.Parameters.AddWithValue("@work_network", _network ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_keyword", _record.keyword_to_use ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_url_form", _record.url_form ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_url_initial", _record.url_initial ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_session_id", _record.session_id ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_customer_record_id", _record.customer_record_id_to_use ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_customer_record_id_2", _record.customer_record_id_2 ?? (object)DBNull.Value);
                            _command.Parameters.AddWithValue("@work_referred_by", _record.referred_by ?? (object)DBNull.Value);

                            string _work_ip_address = _record.ip_address;
                            if (!string.IsNullOrEmpty(_work_ip_address))
                            {
                                if (_work_ip_address.Length>50)
                                {
                                    _work_ip_address = _record.ip_address.Substring(0, 25);
                                }
                            }
                            _command.Parameters.AddWithValue("@work_ip_address", _work_ip_address ?? (object)DBNull.Value);

                            _command.Parameters.AddWithValue("@work_esmid", _record.esmid ?? (object)DBNull.Value);

                            try
                            {

                                _command.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                _logger.Error(string.Format("Error inserting record #{0}: {1}", _count, ex.Message));
                                _error = true;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    _logger.Error("Unable to find /fmresultset/resultset/record elements");
                    _error = true;
                }

                
            }
            catch (Exception ex)
            {

                _error = true;
                string _msg = string.Format("Error: {0}: {1}: {2}", db, _current_record_id, ex.Message);
                _logger.Error(_msg);
            }


            _logger.Info(string.Format("Processed records: {0}", _count));

            _logger.Info("Done");
            _logger.Info("");

            return (_count >0 && !_error);
        }

        static string getFilemakerXMLURL(string db)
        {
            string _url = "";

            _url = "http://esmfmsvr01.esm.local/fmi/xml/fmresultset.xml?";
            _url += "-db=" + db;
            // _url += "&-lay=data";
            _url += "&-lay=pop_mail_export";
            _url += string.Format("&pop_date={0}", _startdate.ToString("MM/dd/yyyy"));
            _url += "&pop_date.op=gte";
            //_url += string.Format("&pop_date={0}", _enddate.ToString("MM/dd/yyyy"));
            //_url += "&pop_date.op=lte";
            //_url += "&lop=and";
            _url += "&-find";

            return _url;

        }

        static IEnumerable<FilemakerRecord> GetFilemakerRecords(XDocument xdoc)
        {
            var _query = from t in xdoc.Elements("fmresultset").Elements("resultset").Elements("record")
                         select new FilemakerRecord
                         {
                             record_id = (int)t.Attribute("record-id"),
                             mod_id = (int)t.Attribute("mod-id"),
                             
                             id_num = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "id_num"),
                             unique_id = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "unique_id"),
                             company = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "company"),
                             campaign_id = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "campaign_id"),
                             campaign = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "campaign"),
                             campaign_name = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "campaign_name"),
                             fm_timestamp = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "timestamp"),
                             fm_timestamp_calc = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "timestamp_calc"),
                             url_from = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "url_from"),
                             site_id = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "site_id"),
                             site = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "site"),
                             search_engine = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "search_engine"),
                             
                             campus_parsed = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "campus_parsed"),
                             campus = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "campus"),
                             program_parsed = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "program_parsed"),
                             program = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "program"),
                             education_parsed = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "education_parsed"),
                             education = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "education"),
                             graduation_parsed = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "graduation_parsed"),
                             graduation = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "graduation"),
                             begin_study_parsed = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "begin_study_parsed"),
                             begin_study = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "begin_study"),
                             contact_time_parsed = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "contact_time_parsed"),
                             contact_time = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "contact_time"),
                             consent = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "consent"),

                             sce = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "sce"),
                             source_code = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "source_code"),
                             source = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "source"),
                             billable = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "billable"),
                             billable_notes = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "billable_notes"),
                             lead_provider = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "lead_provider"),
                             lead_type = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "lead_type"),

                             first_name = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "first_name"),
                             last_name = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "last_name"),
                             email = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "email"),
                             address = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "address"),
                             address2 = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "address2"),
                             city = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "city"),
                             state_parsed = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "state_parsed"),
                             state = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "state"),
                             zip = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "zip"),
                             latitude = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "latitude"),
                             longitude = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "longitude"),
                             phone_1 = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "phone_1"),
                             phone_2 = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "phone_2"),

                             pop_date = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "pop_date"),
                             pop_time = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "pop_time"),
                             pop_date_text = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "pop_date_text"),

                             pop_body_type = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "pop_body_type"),
                             pop_body = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "pop_body"),
                             pop_from = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "pop_from"),
                             pop_full_headers = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "pop_full_headers"),
                             pop_to = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "pop_to"),
                             pop_subject = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "pop_subject"),

                             smtp_from = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "smtp_from"),
                             smtp_subject = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "smtp_subject"),
                             smtp_to = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "smtp_to"),
                             smtp_body = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "smtp_body"),
                             phone_1_label = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "phone_1_label"),
                             phone_2_label = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "phone_2_label"),

                             field_18_label = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "field_18_label"),
                             field_19_label = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "field_19_label"),
                             field_20_label = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "field_20_label"),
                             field_21_label = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "field_21_label"),
                             field_22_label = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "field_22_label"),
                             field_23_label = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "field_23_label"),

                             field_18 = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "field_18"),
                             field_19 = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "field_19"),
                             field_20 = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "field_20"),
                             field_21 = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "field_21"),
                             field_22 = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "field_22"),
                             field_23 = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "field_23"),

                             network = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "network"),
                             keyword = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "keyword"),
                             keywords = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "keywords"),
                             ip_address = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "ip_address"),
                             url_form = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "lead_url_form"),
                             url_initial = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "lead_url_initial"),
                             session_id = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "php_session_id"),
                             customer_record_id = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "customer_record_id"),
                             customer_record_id_2 = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "customer_record_id_2"),
                             referred_by = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "referred_by"),
                             esmid = (string)t.Elements("field").FirstOrDefault(x => x.Attribute("name").Value == "esmid")
                         };

            return _query.ToList();

        }
        



    }
}
